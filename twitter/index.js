/**
 * Thin wrapper around twitter-1.1 api library
 * This could go into a separate node module
 * TODO: Use of `q` is quite messy, get rid of it and create a simple
 * callback based client
**/

var q = require('q');
var config = require('../config');
var twitter = require('twitter-1.1');
var twit = new twitter(config);

var api = {};



api.getFriends = function getFriends(twitter_handle, cursor){
    var deferred = q.defer();

    twit.get('/friends/ids.json', {stringify_ids:true, cursor:cursor || -1, screen_name: twitter_handle},
      function(data){
        if (data.statusCode > 299) return deferred.reject(data);
        deferred.resolve(data);
      });

    return deferred.promise;
}

api.getFollowers = function getFollowers(twitter_handle, cursor){
    var deferred = q.defer();

    twit.get('/followers/ids.json', {stringify_ids:true, cursor:cursor || -1, screen_name: twitter_handle},
      function(data){
        if (data.statusCode > 299) return deferred.reject(data);
        deferred.resolve(data);
      });

    return deferred.promise;
}


/**
 * An optimized friends and followers fetcher. It keeps track of the flow
 * of both friends and followers. If one fails, it stops fetching the other
 * also.
**/
api.getFriendsAndFollowers = function(twitter_handle){
    var erroredOut = false;
    var deferred = q.defer();
    var friendsCursor = -1;
    var followersCursor = -1;
    var friends = [];
    var followers = [];

    function check(){ 
        if(friendsCursor === 0 && followersCursor === 0)
            return deferred.resolve({friends: friends, followers: followers});
    }

    function getFriends(){
        if (erroredOut) return;
        if (friendsCursor === 0) return check();

        api.getFriends(twitter_handle, friendsCursor)
            .then(function(data){
                friendsCursor = data.next_cursor;
                if(Array.isArray(data.ids)) friends = friends.concat(data.ids);
                getFriends();
            })
            .fail(function(err){
                erroredOut = true;
                deferred.reject(err);
            });
    }

    function getFollowers(){
        if (erroredOut) return;
        if (followersCursor === 0) return check();

        api.getFollowers(twitter_handle, followersCursor)
            .then(function(data){
                followersCursor = data.next_cursor;
                if(Array.isArray(data.ids)) followers = followers.concat(data.ids);
                getFollowers();
            })
            .fail(function(err){
                erroredOut = true;
                deferred.reject(err);
            });
    }

    getFriends();
    getFollowers();

    return deferred.promise;
}

/**
 * Synchronous
**/
api.calculateNonFollowers = function(friends, followers){
    var nonFollowers = [];
    // For constant time look up.
    // Overall performance linear in O(friends + followers)
    // But the operation is blocking
    var followersMap = {};
    for (var i = followers.length-1; i >= 0 ; i--) {
      followersMap[followers[i]] = followers[i];
    };

    for (var i = friends.length - 1; i >= 0; i--) {
      if (!followersMap[friends[i]]) nonFollowers.push(friends[i]);
    };
    return nonFollowers;
}

api.hydrate = function(ids, include_entities){
    include_entities = include_entities || false;
    var deferred = q.defer();

    twit.post('/users/lookup.json', {user_id: ids.join(","), include_entities: false}, 
      function(data){
        if (data.statusCode && data.statusCode > 299)
            return deferred.reject(data);

        deferred.resolve(data);
      });

    return deferred.promise;
}

module.exports = api;