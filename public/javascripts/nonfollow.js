(function(){
  var target, more, spinner, container, twitter_handle;
  var cursor = 0;
  var limit = 10;
  
  $(function(){
    container = $("#target")
    target = $("#list");
    spinner = $("#spinner").detach();
    more = $("<a>").attr({id: "more", href: "#"}).text("More");
  
    $("#form").submit(function(e){
      e.preventDefault();
      target.empty();
      more.detach();
      container.append(spinner);
      twitter_handle = document.getElementById('inp').value;
      if (twitter_handle[0] === "@") twitter_handle = twitter_handle.substring(1);
  
      $.ajax({
                url: "/non-followers/" + twitter_handle, 
                success: success,
                error: failure
            });
    });
  
    more.click(function(e){
      e.preventDefault();
      show()
    });
  });
  
  function show(){
    more.detach();
    var data = window.data;
    if(cursor >= data.length) {
      return target.append("Nothing more to show");
    }
    for (var i = cursor; i < cursor+limit && i < data.length; i++) {
      var nonFollower = data[i];
      var li = $("<li>");
      var image = $("<img>").attr("src", nonFollower.profile_image_url);
      var linkToTwitter = $("<a>").attr({
                                          target: "_blank", 
                                          href:"https://twitter.com/"+nonFollower.screen_name, 
                                          title: nonFollower.screen_name
                                        });
  
      var status = $("<span>").text(nonFollower.status && nonFollower.status.text);
  
      linkToTwitter.append(image);
      li.append(linkToTwitter).append(status);
      target.append(li);
    }
    spinner.detach();
    cursor += limit;
  
    more.appendTo("#target");
  }
  
  function success(data, status, jqXHR){
    cursor = 0;
    window.data = data;
    $("title").text(data.length + " non followers for " + twitter_handle);
    show();
  }
  
  function failure(jqXHR, status){debugger
    target.append(jqXHR.responseJSON.data);
    spinner.detach();
  }
})();