# Twitter non followers

## How to:
* clone this repository or extract the tarball in a directory
* `cd tw`
* `npm install`
* `cp config.json.tmpl config.json`
* Open config.json and provide twitter app consumer key and access token
* Meanwhile make sure a mongodb instance is running on localhost:27017. If it isn't it will still work, just it won't save anything to db.
* `npm start` OR `node app.js`
* Go to http://localhost:3000/ and follow the instructions.

## Test
This project is covered exhaustively with test cases. See `test` folder for details. To run the test simply:
* clone this repository or extract the tarball in a directory
* `cd tw`
* `npm install`
* Open config.json and provide twitter app consumer key and access token
* `npm test` OR `mocha`