
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var path = require('path');
var util = require('util');
var mongo = require('mongoskin');
var db = mongo.db('localhost:27017/tw?auto_reconnect=true');
var async = require('async');
var DEBUG = true;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());

/**
 * During performance testing either set NODE_ENV environment variable to "production"
 * or comment the following line, cuz console.log is a blocking output call 
*/
if (process.env.NODE_ENV != "production") app.use(express.logger('dev'));

app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/**************** Code begins here *******************/

app.get('/', routes.index);
var tw = require('./twitter');

app.get('/non-followers/:twitter_username', function(req, res){
  var twitter_handle = req.params.twitter_username;
  tw.getFriendsAndFollowers(twitter_handle)
    .then(function(result){
      var friends = result.friends;
      var followers = result.followers;

      // Warning: Blocking operation
      var nonFollowers = tw.calculateNonFollowers(friends, followers);

      db.collection('nonfollowers').save({_id: twitter_handle, "nonfollowers": nonFollowers}, {upsert: true, strict: true}, 
        function(err){
          // Even if db call fails, there is no reason for not sharing the result with the user anyway.
          // If db call fails, no action taken.
          // Warning console.log is blocking.
          if(err) console.error(err);
        });

      tw.hydrate(nonFollowers)
        .then(function(nonFollowerObjects){
          /**
           * Possible optimization: If saving to database not required simpy make a rest call
           * for hydrate and pipe it to response. No buffer->string->buffer conversion required.
           * We can still save one string->buffer conversion if piped streams emits `data` 
           * event. Need to check. Also can't use this twitter client, since we need the reference
           * of server response object. 
          **/
          res.json(nonFollowerObjects);
        })
          .fail(function(err){
            res.status(500);
            res.json(err);
          });
    })
      .fail(function(err){
        res.status(500);
        res.json(err);
      });

});

module.exports = app;