/**
 * Tests the REST endpoints of http server
**/

var request = require('supertest');
var chai = require('chai');
var app = require('../expressApp')
chai.should();

describe("home path", function(){
  it('should respond with 200', function(){
    request(app)
      .get("/")
      .expect(200)
      .expect('content-type', /html/);
  });
});



describe("Static files", function(){
  describe("javascript files", function(){
    it("should provide nonfollow.js", function(){
      request(app)
        .get("/javascripts/nonfollow.js")
        .expect(200)
        .expect('content-type', 'application/javascript');
    });
  });

  describe("stylesheets", function(){
    it("should provide style.css", function(){
      request(app)
        .get("/stylesheets/style.css")
        .expect(200)
        .expect('content-type', 'text/css');
    });
  });

  describe("images", function(){
    it("should provide spinner.gif", function(){
      request(app)
        .get("/images/spinner.gif")
        .expect(200)
        .expect('content-type', 'image/gif');
    });
  });
});



/**
 * Following are integration tests. To convert them into unit test
 * a stub needs to be created for twitter api.
 * A stub that responds with fixture data on api calls.
**/
describe("/non-followers/twitter_handle", function(){
  it('should respond with 200, json, correct number of non followers', function(done){
    this.timeout(10000);
    request(app)
      .get("/non-followers/juzer_ali")
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res){
        res.should.not.be.null;
        var nonfollowers = res.body;debugger

        nonfollowers.should.be.an.instanceof(Array)
        
        //I have got 35 non followers on twitter
        //This test is fragile, need a dummy twitter api
        //to respond with fixture data
        nonfollowers.length.should.equal(35); 
        done();
      });
  });
});