/**
  * Integration tests for twitter API client. To convert them 
  * into unit tests, need a stub which responds with fixture 
  * data to API endpoints.
**/

var tw = require('../twitter');
var chai = require('chai');
chai.should();

describe("Twitter Client", function(){
  describe("API Object", function(){
    it("should have method getFriends", function(){
     tw.should.include.key('getFriends');     
    });

    it("should have method getFollowers", function(){
     tw.should.include.key('getFollowers');     
    });

    it("should have method getFriendsAndFollowers", function(){
     tw.should.include.key('getFriendsAndFollowers');     
    });
  });



  describe("tw.getFollowers", function(){
    var getFollowers = tw.getFollowers;

    it("should fetch followers of given twitter user", function(done){
      this.timeout(10000);
      getFollowers("juzer_ali", -1)
        .then(function(data){
          data.ids.should.be.an.instanceof(Array);
          (data.ids).length.should.equal(46); //fragile, needs a stub
          (data.next_cursor).should.exist.and.should.be.a.number;
          done();
        })
        .fail(function(err){
          done(err);
        });
    });
  });



  describe("tw.getFriends", function(){
    var getFriends = tw.getFriends;

    it("should fetch friends of given twitter user", function(done){
      this.timeout(10000);
      getFriends("juzer_ali", -1)
        .then(function(data){
          data.ids.should.be.an.instanceof(Array);
          (data.ids).length.should.equal(62); //fragile, needs a stub
          (data.next_cursor).should.exist.and.should.be.a.number;
          done();
        })
        .fail(function(err){
          done(err);
        });
    });
  });



  describe("tw.getFriendsAndFollowers", function(){
    var getFriendsAndFollowers = tw.getFriendsAndFollowers;

    it("should fetch friends and followers of given twitter user", function(done){
      this.timeout(10000);
      getFriendsAndFollowers("juzer_ali")
        .then(function(data){
          var friends = data.friends;
          var followers = data.followers;
          friends.should.be.an.instanceof(Array);
          friends.should.have.length(62);

          followers.should.be.an.instanceof(Array);
          followers.should.have.length(46);
          done();
        })
        .fail(function(err){
          done(err);
        });
    });
  });



  describe("tw.hydrate", function(){
    var hydrate = tw.hydrate;

    it("should fetch twitter user objects corresponding to given twitter ids", function(done){
      var ids = [78798270, 14274428];
      hydrate(ids, false)
        .then(function(users){
          users.length.should.equal(ids.length);
          users[0].screen_name.toLowerCase().should.equal("juzer_ali");
          users[1].screen_name.toLowerCase().should.equal("nischalshetty");
          done();
        }).fail(function(err){
          done(err);
        });
    });
  });



  describe("tw.calculateNonFollowers", function(){
    var calculateNonFollowers = tw.calculateNonFollowers;

    it("api should return an array", function(){
      calculateNonFollowers([1,2,3,4], [1,2,3,4]).should.be.an.instanceof(Array);
    });

    it('should return empty array when friends and followers are same', function(){
      calculateNonFollowers([1,2,3,4], [1,2,3,4]).should.be.empty;      
    });

    it('should return the list of non-followers 1', function(){
      calculateNonFollowers([1,2,3,4], [1,2,3]).should.contain(4);
    });

    it('should return the list of non-followers 2', function(){
      var result = calculateNonFollowers([4,2,1,3], [3,1,2]);
      result.should.have.length(1);
      result.should.contain(4);
    });

    it('should return the list of non-followers 3', function(){
      calculateNonFollowers([], []).should.be.empty;
    });

    it('should return the list of non-followers 4', function(){
      calculateNonFollowers([], [1,2,3]).should.be.empty;
    });

    it('should return the list of non-followers 5', function(){
      var result = calculateNonFollowers([1,2,3], []);
      result.should.have.length(3);
      result.should.contain(1,2,3);
    });

  });
});